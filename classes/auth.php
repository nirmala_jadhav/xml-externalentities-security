<?php
  $http_only = TRUE;
  $path = "/";
  $lifetime = 60*15;
  $domain = null; // There can be multiple clients using this service/api
  $secure_cookie = TRUE;

  session_set_cookie_params($lifetime, $path, $domain, $secure_cookie, $http_only);
  session_start();
  require('db.php'); 
  require('user.php'); 

  if (isset($username) and isset($password) )
    if (User::login($username,$password)) {
      $_SESSION["admin"]    = User::SITE;
      $_SESSION["user"]     = $username;
      $_SESSION['browser']  = $_SERVER["HTTP_USER_AGENT"];
    }  

  if(!isset($_SESSION["browser"]) || $_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
  	$session_hijack_flag = TRUE;
  }
?>
