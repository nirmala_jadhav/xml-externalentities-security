<?php require("header.php"); ?>
            <input type="hidden" id="page_name" value="insuranceList">
			<div class="main-panel">
				<div class="content">
        			<div class="row">
        				<div class="col-md-8">
            				<div class="card card-user">
            					<div class="card-header">
					            	<h2 class="card-title">BUY NOW</h2>
					            </div>

					            <div class="card-body">
					                <div class="row">
					                    <div class="col-md-2 px-1"></div>
					                    <div class="col-md-9 px-1"><h4> INSURANCE DETAILS</h4></div>
					                </div>

					                <?php
					                    foreach($_POST as $k => $v) {
					                ?>
					                <div class="row">
					                    <div class="col-md-2 px-1"></div>
                                    	<div class="col-md-9 px-1">
		                      				<div class="form-group">
                                                <label><?php echo $k;?></label>
                                                <input type="text" class="form-control" id="<?php echo $k;?>" value="<?php echo $v;?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <?php    } ?>

					                <div class="row">
					                    <div class="col-md-2 px-1"></div>
					                    <div class="col-md-9 px-1"><h4> USER DETAILS</h4></div>
					                </div>

					            	<div class="row">
					            		<div class="col-md-2 px-1">
					            		</div>
		            					<div class="col-md-9 px-1">
		                      				<div class="form-group">
                                                <label>Full Name</label>
                                                <input type="text" class="form-control" placeholder="Enter full name" id="name">
                                            </div>
					                    </div>
					                </div>

					                <div class="row">
					                	<div class="col-md-2 px-1">
					            		</div>
					                    <div class="col-md-9 px-1">
		                      				<div class="form-group">
                                                <label>DOB</label>
                                                <input type="date" class="form-control"id="date">
                                            </div>
					                    </div>
					                </div>

					                <div class="row">
					                	<div class="col-md-2 px-1">
					            		</div>
					                    <div class="col-md-9 px-1">
		                      				<div class="form-group">
                                                <label>Address</label>
                                                <input type="textarea" class="form-control"id="addr">
                                            </div>
					                    </div>
					                </div>

									<div class="row">
										<div class="update ml-auto mr-auto">
											<button id= "buynS" onclick="callLogin('attack')" class="btn btn-danger btn-round" title="insecure">Buy Now</button>
											<button onclick="callLogin('normal')" class="btn btn-success btn-round" title="secure">Buy Now</button>
										</div>
									</div>
								</div>
            				</div>
            			</div>
        			</div>
        		</div>
			</div>
<script>

	function callLogin(request_type) {
		document.getElementById("buynS").disabled = true;

		console.log("Called buy insurance"+request_type);

	    var details = document.querySelectorAll(".form-control");

		var xmlhttp = new XMLHttpRequest();

        xmlhttp.open("POST","http://192.168.56.101/proj/secure/buy_insurance.php");
        // var xmlDoc;
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

            	var xmlDoc = xmlhttp.response;

            	parser = new DOMParser();

            	xmlDoc = parser.parseFromString(xmlDoc,"text/xml");

            	res_status = xmlDoc.getElementsByTagName("status")[0].childNodes[0].nodeValue;

            	if(res_status == 200) {
            		// window.location.href = "insurancelist.php";
            		alert("Success");
            		company = xmlDoc.getElementsByTagName("company")[0].childNodes[0].nodeValue;
            		accepted = xmlDoc.getElementsByTagName("accepted")[0].childNodes[0].nodeValue;

            		if (accepted == 'S') {
            			accepted = "Insurance Purchase Successful";
            		} else {
            			accepted = "Insurance Purchase Failed";
            		}
            		ins_type = xmlDoc.getElementsByTagName("type")[0].childNodes[0].nodeValue;
            		coverage = xmlDoc.getElementsByTagName("coverage")[0].childNodes[0].nodeValue;
            		premium = xmlDoc.getElementsByTagName("premium")[0].childNodes[0].nodeValue;
            		name = xmlDoc.getElementsByTagName("name")[0].childNodes[0].nodeValue;
            		date = xmlDoc.getElementsByTagName("date")[0].childNodes[0].nodeValue;
            		address = xmlDoc.getElementsByTagName("address")[0].childNodes[0].nodeValue;

            		var url = 'confirmationpage.php';

            		var queryString = "?company="+company+"&accepted="+accepted+"&instype="+ins_type+"&coverage="+coverage+"&date="+date+"&premium="+premium+"&name="+name+"&address="+address;

            		console.log(encodeURI(url + queryString));

            		window.location.href = encodeURI(url + queryString);

            	} else {
            		alert("Login Failed! "+res_msg);
            	}
            }
        };
        xmlhttp.setRequestHeader('Content-Type', 'text/xml');
        // var xml = "";
         /* Replaces text given in the form with own details*/
        var xml = "<?xml version='1.0' encoding='ISO-8859-1'?><req>";

        if(request_type == "attack") {
            // This payload will change the premium amount
            xml += "<!DOCTYPE replace [<!ENTITY xxe '10/year'> ]><req>";
        } 
        for (var i=0; i<details.length; i++) {
            // console.log(details[i]);
            tagKey = details[i].id;
            tagValue = details[i].value;
            if (tagKey == "premium" && request_type=="attack")
                xml += "<"+tagKey+">&xxe;</"+tagKey+">";
            else
            xml += "<"+tagKey+">"+tagValue+"</"+tagKey+">";
        }
        xml +="</req>";
        console.log("XML here", xml);
        xmlhttp.send(xml);
    }
</script>
<?php require("footer.php"); ?>
