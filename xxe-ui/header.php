<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
		<!-- CSS Files -->
		<link href="http://localhost/latestlbs/xxe-ui/bootstrap.min.css" rel="stylesheet" />
		<link href="http://localhost/latestlbs/xxe-ui/paper-dashboard.min.css" rel="stylesheet" />
	</head>
	<body>
		<div class="wrapper ">
			<div class="sidebar" data-background-color="white" data-active-color="danger">
			    <div class="sidebar-wrapper" style="height: 100vh;">
			      <div class="logo">
			        <a href="#" class="simple-text">
			          XML External Entity
			        </a>
			      </div>

			      <ul class="nav">
			        <li id="menu-insuranceList">
			          <a href="./insurancelist.php">
			            <i class="ti-panel"></i>
			            <p>Insurance List</p>
			          </a>
			        </li>
			        <li id="menu-login">
			          <a href="user.html">
			            <i class="ti-user"></i>
			            <p>Logout</p>
			          </a>
			        </li>
			        <!-- <li id="menu-createAccount">
			          <a href="table.html">
			            <i class="ti-view-list-alt"></i>
			            <p>Create Account</p>
			          </a>
			        </li>
			        <li id="menu-addInsurance">
			          <a href="typography.html">
			            <i class="ti-text"></i>
			            <p>Add Insurance</p>
			          </a>
			        </li> -->
			      </ul>
			    </div>
			</div>