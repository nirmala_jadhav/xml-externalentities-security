<?php require("../header.php"); ?>
			<input type="hidden" id="page_name" value="insuranceList">
			<div class="main-panel">
				<div class="content">
        			<div class="row">
        				<div class="col-md-12">
            				<div class="card card-user">
            					<div class="card-header">
					            	<h5 class="card-title">Insurance Status</h5>
					            </div>

					            <div class="card-body">
					            	<?php

					            		foreach ($_GET as $key => $value) {
					            		?>
					            		<div class="row">
						            		<div class="col-md-2 px-1">
						            		</div>
			            					<div class="col-md-9 px-1">
			                      				<div class="form-group">
			                      				<label><?php echo $key; ?></label>
						                        <input type="text" class="form-control" value="<?php echo $value;?>" disabled>
						                      </div>
						                    </div>
						                </div>
					            		<?php
					            		}
									?>
								</div>
            				</div>
            			</div>
        			</div>
        		</div>
			</div>
<?php require("../footer.php"); ?>