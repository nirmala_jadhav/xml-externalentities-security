<?php
	//Curl reqquest
	$ch = curl_init();
    $headers = array(
    	'Accept: text/xml',
    	'Content-Type: text/xml');
    curl_setopt($ch, CURLOPT_URL, "http://192.168.56.101/proj/non-secure/get_insurance.php");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

    $insuranceList = curl_exec($ch);
?>

<?php require("../header.php"); ?>
			<input type="hidden" id="page_name" value="insuranceList">
			<div class="main-panel">
				<div class="content" id="cont">
        			<div class="row">
        				<div class="col-md-12">
            				<div class="card card-user">
            					<div class="card-header">
					            	<h2>INSURANCE LIST changed</h2>
					            </div>

					            <div class="card-body">
					            	<div class="row">
		            					<div class="col-md-12 px-1">
		                      				<table class="table table-border">
		                      				    <thead>
		                      				        <th>Company</th>
		                      				        <th>Type</th>
		                      				        <th>Coverage</th>
		                      				        <th>Premium</th>
		                      				        <th></th>
		                      				    </thead>
		                      				    <tbody>
		                      				    	<?php if(empty($insuranceList)) {
		                      				    			echo "No records Found";
		                      				    		} else {
		                      				    			$xml_op = simplexml_load_string($insuranceList, "SimpleXMLElement", LIBXML_NOCDATA);
															$json = json_encode($xml_op);
															$op_array = json_decode($json,TRUE);
		                      				    			foreach ($op_array['insurance_list']['insurance'] as $key => $value) {
															?>
																<tr id="insurance-<?php echo $key+1;?>">
																	<td name="insurance_id" style="display:none"><?php echo $value['insurance_id'];?></td>
					                      				            <td name="company"><?php echo $value['company'];?></td>
					                      				            <td name="type"><?php echo $value['type'];?></td>
					                      				            <td name="coverage"><?php echo $value['coverage'];?></td>
					                      				             <td name="premium"><?php echo $value['premium']."/".$value['payment_type'];?></td>
					                      				            <td>
					                      				                <button id="<?php echo $key+1;?>" onclick="buy('non-secure',<?php echo $key+1;?>)" class="btn btn-primary btn-round">Buy</button>
					                      				            </td>
					                      				        </tr>
															<?php

	                      				    				}	
		                      				    		}
		                      				    	?>
		                      				    </tbody>
		                      				</table>
					                    </div>
					                </div>
								</div>
            				</div>
            			</div>
        			</div>
        		</div>
			</div>
<script>
	function buy(request_type, id) {
		var sel_insurance = document.getElementById("insurance-"+id);

        // Created form to post and redirect to this URL
        var elemDiv = document.createElement('form');
        elemDiv.id="container";
        elemDiv.action="buyform.php";
        elemDiv.method="post";
        document.body.append(elemDiv);

		for (let i = 0; i < sel_insurance.children.length; i++) {
          var child = sel_insurance.children[i];
          var name = child.getAttribute('name');
          if (name != null) {
                var input = document.createElement("input");
                input.type = "text";
                input.name = name;
                input.value = child.innerHTML.split('"').join('\"');

                var container = document.getElementById('container');
                console.log(container);
                container.appendChild(input);
          }
        }
        document.getElementById("container").submit();
    }
</script>
<?php require("../footer.php"); ?>
