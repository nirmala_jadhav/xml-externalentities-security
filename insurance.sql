use insurance;

CREATE TABLE `login` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

insert into login (username, password) values ("admin",password("admin2021"));

insert into login (username, password) values ("jadhavn1",password("jadhavn1"));

insert into login (username, password) values ("patyarab1",password("patyarab1"));


CREATE TABLE `purchases` (
  `purchase_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `insurance_company` varchar(50) DEFAULT NULL,
  `insurance_type` varchar(50) DEFAULT NULL,
  `coverage` mediumint(50) DEFAULT 0,
  `premium` double(12,2) DEFAULT 0.00,
  `fullname` varchar(50) DEFAULT NULL,
  `user_address` varchar(50) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `status` char(1) DEFAULT 'F',
  PRIMARY KEY (`purchase_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `insurance_details`(
  `insurance_id` mediumint(9) NOT NULL AUTO_INCREMENT, 
  `insurance_company` varchar(50) DEFAULT NULL,           `insurance_type` varchar(50) DEFAULT NULL, 
  `coverage` mediumint(9) DEFAULT 0, 
  `premium` double(12,2) DEFAULT 0.00,
  `payment_type` varchar(50) DEFAULT "year" 
  PRIMARY KEY (`insurance_id`)
);

INSERT INTO insurance_details (insurance_company,insurance_type, coverage, premium, payment_type) values
("PSI", "Health", 500000, 800, "year"),
("Aetna", "Health", 500000, 100, "month"),
("Progressive", "Auto", 100000, 100, "month");