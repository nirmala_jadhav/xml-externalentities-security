# LANGUAGE BASED SECURITY #

## XML External Entities and Security

## Project Objectives and Plan ##

Demonstrate XML external entity attack and prevent the attack using the above-mentioned prevention techniques.


### Plan ###

* Phase 1 : System Requirement, XML attack demonstration, XML prevention techniques in code[input validation, less complex data structures]
* Phase 2 : Web application which accepts XML, XML prevention[Dependency checker, upgrade of XML processor etc.], XXE prevention tools at developer�s side, XML attack prevented demonstration


# Project Accomplishment #

### System Requirements ###

* Server 		- Apache server
* Server side scripting - PHP
* Client side 		- [HTML, CSS, Javascript] Application which calls server side scripts
* Request parser        - Simple XML

### Completed Tasks ###

* Designed and Developed Basic web page at client side to call server script
* Created a Server side script to demonstrate attack
* Tested a basic attack with XML on a single script
* Documented the payload used for attack
* Server response changed from JSON to XML
* Insurance purchase status
* Demonstrated XXE attack from client to server 
* Prevented XXE attack on secure server with XXE prevention techniques

# Installation and Configuration 

* For server side - Apache server, PHP 7.0 and Mysql is required
* For client side - Apache sever, PHP 7.0 and browser is required
* Create folder proj in local server[/var/www/html]
* Copy classes, secure and non-secure folder into it
* For client side, create folder XXE in local server[/var/www/html]
* Copy xxe-ui folder into it
* Access the same from browser http://localhost/XXE/xxe-ui/
