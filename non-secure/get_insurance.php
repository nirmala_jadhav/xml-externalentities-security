<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: *');

	session_start();
  require('../classes/auth.php');
	require('../classes/db.php'); 
	require ('../classes/insurance.php');

  	$output = Insurance::get_insurance_list();
  	// error_log($res);
  	$response = '<?xml version="1.0" encoding="utf-8"?>';
  	if(!empty($output)) {
  		$response .= '<res><status>200</status><insurance_list>';
  		$count = $output->num_rows;
  		while ($count > 0) {
  			$value = mysqli_fetch_assoc($output);
  			error_log($value['insurance_company']);
  			$response .= '<insurance>';
        $response .= '<insurance_id>'.$value['insurance_id'].'</insurance_id>'; 
  			$response .= '<company>'.$value['insurance_company'].'</company>'; 
  			$response .= '<type>'.$value['insurance_type'].'</type>';
  			$response .= '<coverage>'.$value['coverage'].'</coverage>';
  			$response .= '<premium>'.$value['premium'].'</premium>';
  			$response .= '<payment_type>'.$value['payment_type'].'</payment_type>';
  			$response .= '</insurance>';
  			$count--;
  		}
  		$response .= '</insurance_list></res>';
  	} else {
  		$response .= '<res><status>200</status>';
        $response .= '<insurance_list></insurance_list></res>';
  	}
  	error_log("Response Final ".$response);
  	header('Content-type: text/xml; charset=utf-8');
    echo $response;
    exit;
?>