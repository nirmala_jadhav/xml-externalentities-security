<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: *');
	libxml_disable_entity_loader(false);

	// XML post sent in a request
    $xmlfile = file_get_contents('php://input');

    // Comvert and load as an XML document
    $dom = new DOMDocument();
    $dom->loadXML($xmlfile, LIBXML_NOENT | LIBXML_DTDLOAD);

    // Parsing xml using simplexml
    $creds = simplexml_import_dom($dom);

    // Accessing the values sent in xml
    $username = $creds->user;
    $password = $creds->pass;
    $session_hijack_flag = FALSE;

    require ('../classes/auth.php');
    $response = '<?xml version="1.0" encoding="utf-8"?>';
    if(isset($_SESSION['user']) && !$session_hijack_flag) {
        $response .= '<res><status>200</status>';
        $response .= '<message>Successful login for user '.$username.'</message></res>';
    } else {
        $response .= '<res><status>400</status>';
        $response .= '<message>Failed login for user '.$username.'</message></res>';
    }
    header('Content-type: text/xml; charset=utf-8');
    echo $response;
    exit;
?>
