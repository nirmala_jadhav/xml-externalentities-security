<?php 
    require '../classes/db.php';
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: *');
	libxml_disable_entity_loader(false);

	/* // Disable external entity support 
	libxml_disable_entity_loader(false);
	libxml_set_external_entity_loader(null);*/

	// XML post sent in a request
    $xmlfile = file_get_contents('php://input');

    // Comvert and load as an XML document
    $dom = new DOMDocument();
    $dom->loadXML($xmlfile, LIBXML_NOENT | LIBXML_DTDLOAD);

    // Parsing xml using simplexml
    $creds = simplexml_import_dom($dom);

    // Accessing the values sent in xml
    $username = $creds->user;
    $password = $creds->pass;
    $response = '<?xml version="1.0" encoding="utf-8"?>';

    // Validation Rules on username
	    // Non empty
	   	// Allowed chars alpahabets, numbers @ and .
	   	// Length of username not more than 50 characters
    // Validation Rules on password
        // Non empty
        // Length of password not more than 70 characters
    if(empty($username) || strlen($username)>50 || preg_match('/[^a-z@.0-9]/i', $username) || empty($password) || strlen($password)>70) {
        $response .= '<res><status>401</status>';
        $response .= '<message>Sorry! could not process request. Invalid credentials</message></res>';
    } else {

        // Check in DB
        $sql = "SELECT * FROM login WHERE username='$username' AND password=password('$password');";
        $result = mysqli_query($dblink, $sql);
        if($result) {
            $row = mysqli_fetch_assoc($result);
            if ($username == $row['username']) {
                $response .= '<res><status>200</status>';
                $response .= '<message>Successful login for user '.$username.'</message></res>';
            } else {
                $response .= '<res><status>402</status>';
                $response .= '<message>Sorry! could not process request. Invalid credentials</message></res>';
            }
        } else {
            $response .= '<res><status>400</status>';
            $response .= '<message>Failed login for user '.$username.'</message></res>';
        }
    }

    header('Content-type: text/xml; charset=utf-8');
    echo $response;
    exit;
	
?>
