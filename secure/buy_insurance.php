<?php
    require '../classes/db.php';
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Headers: *');
    libxml_disable_entity_loader(false);

    if (!isset($_POST)) {
        error_log("I think open is culprit");
        exit;
    } 

	// XML post sent in a request
    $xmlfile = file_get_contents('php://input');

    // Comvert and load as an XML document
    $dom = new DOMDocument();
    $dom->loadXML($xmlfile, LIBXML_NOENT | LIBXML_DTDLOAD);

    // Parsing xml using simplexml
    $req = simplexml_import_dom($dom);

    $company =(string)$req->company;
    $type = (string)$req->type;
    $coverage = (string)$req->coverage;
    $premium = (string)$req->premium;
    $name =(string)$req->name;
    $date =(string)$req->date;
    $addr =(string)$req->addr;
    $insurance_id =(string)$req->insurance_id;
    $status = "";

    // Check if DOB is valid and reject and accept on basic of that
    if($date > date('Y-m-d'))
        $status = "F";
    else
        $status = "S";

    // Check in DB
    require('../classes/auth.php');
	require('../classes/db.php'); 
	require ('../classes/insurance.php');
    $premium_arr = explode("/", $premium);
    $premium_val = $premium_arr[0];
    $check_pre = Insurance::check_premium($insurance_id, $premium_val);

    if (!$check_pre) {
        $status = "F";
    }
    $sql = Insurance::insert_purchase($company, $type, $coverage, $premium_val, $name, $date, $addr, $status);
    // $result = mysqli_query($dblink, $sql);

    $response = '<?xml version="1.0" encoding="utf-8"?>';

    // Printing the user entered details
    // $res = ["status"=>$status, "company"=>$company, "type"=>$type, "coverage"=>$coverage, "premium"=>$premium, "name"=>$name, "date"=>$date, "addr"=>$addr];

    $response .= '<res><status>200</status>';
    $response .= '<accepted>'.$status.'</accepted>';
    $response .= '<company>'.$company.'</company>';
    $response .= '<type>'.$type.'</type>';
    $response .= '<coverage>'.$coverage.'</coverage>';
    $response .= '<premium>'.$premium_val.'</premium>';
    $response .= '<name>'.$name.'</name>';
    $response .= '<date>'.$date.'</date>';
    $response .= '<address>'.$addr.'</address></res>';

    error_log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^".$response);

    header('Content-type: text/xml; charset=utf-8');
    echo $response;
    exit;

?>